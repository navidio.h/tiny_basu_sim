#ifndef __tiny_basu_sim_h
#define __tiny_basu_sim_h

#include <stdint.h>

#define REG_CNT 8
#define MEMORY_SIZE 512
#define BPT_LEN 2
#define FIELD_CNT 10
#define FIELD_LEN 20
#define LABEL_CNT 10
#define LABEL_LEN 10
#define LINE_LEN 100
#define RT_OP 0
#define ADDI_OP 1
#define LI_OP 2
#define LUI_OP 3
#define LW_OP 4
#define SW_OP 5
#define SLL_OP 6
#define SRL_OP 7
#define ANDI_OP 8
#define ORI_OP 9
#define BEQ_OP 10
#define BNE_OP 11
#define BGT_OP 12
#define BLT_OP 13
#define JMP_OP 14
#define JAL_OP 15
#define MOD_FUNC 0
#define ADD_FUNC 1
#define SUB_FUNC 2
#define MUL_FUNC 3
#define SLT_FUNC 4
#define DIV_FUNC 5
#define AND_FUNC 6
#define OR_FUNC 7
// default cycle counts
// pipelined architecture
#define DEF_CYCLE_CNT 1
// 2-cycle penalty
#define UNSUCCESSFUL_PREDICTION_CYCLE_CNT 3

enum prediction_method {ST, SN, D1, D2, IQ};

typedef struct tb_sim
{
	int16_t reg[REG_CNT];
	int16_t memory[MEMORY_SIZE];
	int16_t pc;
	int16_t BPT[BPT_LEN];
	double runtime;
	enum prediction_method p_method;
	// counts
	int16_t cycle_cnt;
	int16_t no_prediction_cycle_cnt;
	int16_t instruction_cnt;
	int16_t stall_cnt;
	int16_t successful_prediction_cnt;
	int16_t total_prediction_cnt;
} tb_sim;

// clears registers, reinitializes memory and sets default values
void refresh_sim(tb_sim*, const enum prediction_method);

// parses and extracts instructions from the asm file, stores results in memory as machine codes (16-bit integer)
void parse_asm_file(int16_t*, const char*);

// removes all space characters from a string
void remove_spaces(char*);

// reads data from the data file and stores them in memory
void parse_data_file(int16_t*, const char*);

// fetches next instruction from memory and returns it
const int16_t fetch(tb_sim*);

// decodes a machine code and updates the values sent to it to match the current instruction
void decode(const int16_t, int16_t*, int16_t*, int16_t*, int16_t*, int16_t*, int16_t*, int16_t*);

// executes an instruction (updates memory and simulator variables)
// reminder: default number of cycles: 1 (beq and bne may differ)
void execute(tb_sim*, const int16_t);

// performs branch prediction based on the selected method and returns the result
const int16_t branch_prediction(const enum prediction_method, int16_t*);

// updates the branch prediction table based on the current method,
// the current state of the BPT and the result of the previous branch instruction
void update_BPT(const enum prediction_method, int16_t*, const int16_t);

// executes all instructions using the virtual processor
// times out if the execution time exceeds a predefined value
void run(tb_sim*, const int16_t, const enum prediction_method, const char*, const char*, const char*);

// writes simulation results to a report file
void report(const tb_sim*, const char*);

#endif // __tiny_basu_sim_h
