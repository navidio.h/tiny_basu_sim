#include "tiny_basu_sim.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

void refresh_sim(tb_sim* sim, const enum prediction_method meth)
{
	memset(sim->reg, 0, REG_CNT * sizeof(int16_t));
	memset(sim->memory, 0, MEMORY_SIZE * sizeof(int16_t));
	sim->pc = 0;
	sim->BPT[0] = 0, sim->BPT[1] = 0;
	sim->runtime = 0;
	sim->p_method = meth;
	// unlike later instructions, the first instruction takes 3 cycles,
	// so this offset compensates for the 2 extra cycles
	sim->cycle_cnt = 2;
	sim->no_prediction_cycle_cnt = 2;
	sim->instruction_cnt = 0;
	sim->stall_cnt = 0;
	sim->successful_prediction_cnt = 0;
	sim->total_prediction_cnt = 0;
}

void remove_spaces(char* str)
{
	int16_t index = 0;
	for (int16_t i = 0; str[i] != '\0'; ++i)
		if (str[i] != ' ')
			str[index++] = str[i];
	str[index] = '\0';
}

void parse_asm_file(int16_t* memory, const char* asm_file)
{
	// open the asm file for reading
	FILE* file = fopen(asm_file, "r");
	if (file == NULL)
	{
		puts("failed to open asm file");
		exit(EXIT_FAILURE);
	}
	int16_t machine_code;
	// stores the label itself
	char label[LABEL_CNT][LABEL_LEN];
	// stores the label's line number
	int16_t label_line[LABEL_CNT];
	// stores the last valid label's index
	int16_t valid_label_index = -1;
	// stores a line from the asm file
	char line[LINE_LEN];
	int16_t line_num = -1;
	// stores broken fields of the current instruction
	char field[FIELD_CNT][FIELD_LEN];
	// stores the last valid field's index
	int16_t field_index;
	// stores a field (token) from the line
	char* token;
	// variables for finding labels
	// stores the position of the ':' character
	char* col_pos;
	// stores the index of the ':' character
	int16_t col_index;
	// used for beq, bne, jmp and jal instructions
	int16_t imm;
	while (fgets(line, LINE_LEN, file) != NULL)
	{
		// resetting field index
		field_index = -1;
		// new line
		++line_num;
		remove_spaces(line);
		// will be manipulated to match the current instruction
		machine_code = 0x0000;
		token = strtok(line, ",");
		// tokenizing current instruction (breaking into fields)
		while (token != NULL)
		{
			strncpy(field[++field_index], token, FIELD_LEN - 1);
			field[field_index][FIELD_LEN - 1] = '\0';
			token = strtok(NULL, ",");
		}
		// if an instruction is detected
		if (field_index >= 0)
		{
			// checking for label
			col_pos = strchr(field[0], ':');
			// label found
			if (col_pos != NULL)
			{
				// acts as the length in the copying process
				col_index = col_pos - field[0];
				// copying label into the label matrix
				strncpy(label[++valid_label_index], field[0], col_index);
				label[valid_label_index][col_index] = '\0';
				// storing label's line number
				label_line[valid_label_index] = line_num;
				// moving pointer past the label
				strncpy(field[0], col_pos + 1, FIELD_LEN - 1);
				field[0][FIELD_LEN - 1] = '\0';
			}
			// r-type instructions
			// add (must consider instruction addi)
			if (strncmp(field[0], "addr", 4) == 0)
			{
				// opcode (0 for r-type instructions)
				machine_code += (RT_OP << 12) & 0xF000;
				// rd
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				// rs
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				// rt
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				// func
				machine_code += ADD_FUNC & 0x0007;
			}
			// same process for other r-type instructions
			// sub
			else if (strncmp(field[0], "sub", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += SUB_FUNC & 0x0007;
			}
			// mul
			else if (strncmp(field[0], "mul", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += MUL_FUNC & 0x0007;
			}
			// div
			else if (strncmp(field[0], "div", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += DIV_FUNC & 0x0007;
			}
			// slt
			else if (strncmp(field[0], "slt", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += SUB_FUNC & 0x0007;
			}
			// and
			else if (strncmp(field[0], "and", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += AND_FUNC & 0x0007;
			}
			// or
			else if (strncmp(field[0], "or", 2) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 4) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += OR_FUNC & 0x0007;
			}
			// mod
			else if (strncmp(field[0], "mod", 3) == 0)
			{
				machine_code += (RT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? (atoi(field[2] + 2) << 3) & 0x0038 : 0;
				machine_code += MOD_FUNC & 0x0007;
			}
			// i-type instructions
			// addi
			else if (strncmp(field[0], "addi", 4) == 0)
			{
				// opcode
				machine_code += (ADDI_OP << 12) & 0xF000;
				// rd
				machine_code += (atoi(field[0] + 6) << 9) & 0x0E00;
				// rs
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				// immediate
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// same process for other i-type instructions
			// li (note: no rs)
			else if (strncmp(field[0], "li", 2) == 0)
			{
				machine_code += (LI_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 4) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? atoi(field[1]) : 0;
			}
			// lui (note: no rs)
			else if (strncmp(field[0], "lui", 3) == 0)
			{
				machine_code += (LUI_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? atoi(field[1]) : 0;
			}
			// lw
			else if (strncmp(field[0], "lw", 2) == 0)
			{
				machine_code += (LW_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 4) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// sw
			else if (strncmp(field[0], "sw", 2) == 0)
			{
				machine_code += (SW_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 4) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// sll
			else if (strncmp(field[0], "sll", 3) == 0)
			{
				machine_code += (SLL_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// srl
			else if (strncmp(field[0], "srl", 3) == 0)
			{
				machine_code += (SRL_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// andi
			else if (strncmp(field[0], "andi", 4) == 0)
			{
				machine_code += (ANDI_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 6) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// ori
			else if (strncmp(field[0], "ori", 3) == 0)
			{
				machine_code += (ORI_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				machine_code += (field_index >= 2) ? atoi(field[2]) : 0;
			}
			// beq
			else if (strncmp(field[0], "beq", 3) == 0)
			{
				machine_code += (BEQ_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				if (field_index >= 2)
				{
					if (isdigit(field[2][0]) || field[2][0] == '-')
						imm = atoi(field[2]);
					else
					{
						// removing '\n' from the input label
						(field[2])[strcspn(field[2], "\n")] = '\0';
						for (int16_t i = 0; i <= valid_label_index; ++i)
							if (strcmp(field[2], label[i]) == 0)
							{
								imm = label_line[i] - line_num;
								break;
							}
					}
					// preventing sum operation issues by complementing immediate
					// and setting the sixth bit to the sign (1 for negative numbers)
					if (imm < 0)
						imm = -imm, imm |= 0x0020;
					machine_code += imm;
				}
			}
			// bne
			else if (strncmp(field[0], "bne", 3) == 0)
			{
				machine_code += (BNE_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				if (field_index >= 2)
				{
					if (isdigit(field[2][0]) || field[2][0] == '-')
						imm = atoi(field[2]);
					else
					{
						// removing '\n' from the input label
						(field[2])[strcspn(field[2], "\n")] = '\0';
						for (int16_t i = 0; i <= valid_label_index; ++i)
							if (strcmp(field[2], label[i]) == 0)
							{
								imm = label_line[i] - line_num;	
								break;
							}
					}
					if (imm < 0)
						imm = -imm, imm |= 0x0020;
					machine_code += imm;
				}
			}
			// bgt
			else if (strncmp(field[0], "bgt", 3) == 0)
			{
				machine_code += (BGT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				if (field_index >= 2)
				{
					if (isdigit(field[2][0]) || field[2][0] == '-')
						imm = atoi(field[2]);
					else
					{
						// removing '\n' from the input label
						(field[2])[strcspn(field[2], "\n")] = '\0';
						for (int16_t i = 0; i <= valid_label_index; ++i)
							if (strcmp(field[2], label[i]) == 0)
							{
								imm = label_line[i] - line_num;
								break;
							}
					}
					if (imm < 0)
						imm = -imm, imm |= 0x0020;
					machine_code += imm;
				}
			}
			// blt
			else if (strncmp(field[0], "blt", 3) == 0)
			{
				machine_code += (BLT_OP << 12) & 0xF000;
				machine_code += (atoi(field[0] + 5) << 9) & 0x0E00;
				machine_code += (field_index >= 1) ? (atoi(field[1] + 2) << 6) & 0x01C0 : 0;
				if (field_index >= 2)
				{
					if (isdigit(field[2][0]) || field[2][0] == '-')
						imm = atoi(field[2]);
					else
					{
						// removing '\n' from the input label
						(field[2])[strcspn(field[2], "\n")] = '\0';
						for (int16_t i = 0; i <= valid_label_index; ++i)
							if (strcmp(field[2], label[i]) == 0)
							{
								imm = label_line[i] - line_num;
								break;
							}
					}
					if (imm < 0)
						imm = -imm, imm |= 0x0020;
					machine_code += imm;
				}
			}
			// j-type instructions
			// jmp
			else if (strncmp(field[0], "jmp", 3) == 0)
			{
				// opcode
				machine_code += (JMP_OP << 12) & 0xF000;
				// immediate or label
				if (isdigit(field[0][3]) || field[0][3] == '-')
					imm = atoi(field[0] + 3);
				else
				{
					// removing '\n' from the input label
					(field[0] + 3)[strcspn(field[0] + 3, "\n")] = '\0';
					for (int16_t i = 0; i <= valid_label_index; ++i)
						if (strcmp(field[0] + 3, label[i]) == 0)
						{
							imm = label_line[i] - line_num;
							break;
						}
				}
				// setting the eleventh bit to the sign
				if (imm < 0)
					imm = -imm, imm |= 0x0800;
				machine_code += imm;
			}
			// jal (almost same as jmp)
			else if (strncmp(field[0], "jal", 3) == 0)
			{
				machine_code += (JAL_OP << 12) & 0xF000;
				if (isdigit(field[0][3]) || field[0][3] == '-')
					imm = atoi(field[0] + 3);
				else
				{
					(field[0] + 3)[strcspn(field[0] + 3, "\n")] = '\0';
					for (int16_t i = 0; i <= valid_label_index; ++i)
						if (strcmp(field[0] + 3, label[i]) == 0)
						{
							imm = label_line[i] - line_num;	
							break;
						}
				}
				if (imm < 0)
					imm = -imm, imm |= 0x0800;
				machine_code += imm;
			}
			else
			{
				puts("unrecognized instruction in asm file!");
				exit(EXIT_FAILURE);
			}
			// storing current instruction into memory
			memory[line_num] = machine_code;
		}
	}
	fclose (file);
}

void parse_data_file(int16_t* memory, const char* data_file)
{
	FILE* file = fopen(data_file, "r");
	if (file == NULL)
	{
		puts("failed to open data file");
		exit(EXIT_FAILURE);
	}
	int16_t index = 256;
	char line[LINE_LEN];
	while (fgets(line, LINE_LEN, file) != NULL)
		// 16 for hexadecimal values
		memory[index++] = strtol(line, NULL, 16);
	fclose(file);
}

const int16_t fetch(tb_sim* sim)
{
	if (sim->pc >= 0 && sim->pc < MEMORY_SIZE / 2)
		return sim->memory[sim->pc++];
	else
	{
		puts("pc is out of bounds!");
		exit(EXIT_FAILURE);
	}
}

void decode(const int16_t instr, int16_t* opcode, int16_t* rd, int16_t* rs, int16_t* rt, int16_t* func, int16_t* i_imm, int16_t* j_imm)
{
	// assumed signs of i-type and j-type immediates
	int16_t i_sign = (instr >> 5) & 0x0001, j_sign = (instr >> 11) & 0x0001;
	*opcode = (instr >> 12) & 0x000F;
	*rd = (instr >> 9) & 0x0007;
	*rs = (instr >> 6) & 0x0007;
	*rt = (instr >> 3) & 0x0007;
	*func = instr & 0x0007;
	if (i_sign)
		*i_imm = -(instr & 0x001F);
	else
		*i_imm = instr & 0x001F;
	if (j_sign)
		*j_imm = -(instr & 0x07FF);
	else
		*j_imm = instr & 0x07FF;
}

void execute(tb_sim* sim, const int16_t instr)
{
	int16_t opcode, rd, rs, rt, func, i_imm, j_imm;
	// only used for beq and bne instructions
	int16_t branch_comp_result;
	// first decode the instruction
	decode(instr, &opcode, &rd, &rs, &rt, &func, &i_imm, &j_imm);
	// must determine instruction type based on its opcode
	switch (opcode)
	{
		// r-type
		case RT_OP:
			// add
			if (func == ADD_FUNC)
				sim->reg[rd] = sim->reg[rs] + sim->reg[rt];
			// sub
			else if (func == SUB_FUNC)
				sim->reg[rd] = sim->reg[rs] - sim->reg[rt];
			// slt
			else if (func == SLT_FUNC)
				sim->reg[rd] = (sim->reg[rs] < sim->reg[rt]) ? 1 : 0;
			// mul
			else if (func == MUL_FUNC)
				sim->reg[rd] = sim->reg[rs] * sim->reg[rt];
			// div
			else if (func == DIV_FUNC)
				sim->reg[rd] = sim->reg[rs] / sim->reg[rt];
			// and
			else if (func == AND_FUNC)
				sim->reg[rd] = sim->reg[rs] & sim->reg[rt];
			// or
			else if (func == OR_FUNC)
				sim->reg[rd] = sim->reg[rs] | sim->reg[rt];
			else if (func == MOD_FUNC)
				sim->reg[rd] = sim->reg[rs] % sim->reg[rt];
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// i-type
		// addi
		case ADDI_OP:
			sim->reg[rd] = sim->reg[rs] + i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// li
		case LI_OP:
			sim->reg[rd] = i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// lui
		case LUI_OP:
			sim->reg[rd] = i_imm << 10;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// lw
		case LW_OP:
			sim->reg[rd] = sim->memory[sim->reg[rs] + i_imm + MEMORY_SIZE / 2];
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// sw
		case SW_OP:
			sim->memory[sim->reg[rs] + i_imm + MEMORY_SIZE / 2] = sim->reg[rd];
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// sll
		case SLL_OP:
			sim->reg[rd] = sim->reg[rs] << i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// srl
		case SRL_OP:
			sim->reg[rd] = sim->reg[rs] >> i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// andi
		case ANDI_OP:
			sim->reg[rd] = sim->reg[rs] & i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// ori
		case ORI_OP:
			sim->reg[rd] = sim->reg[rs] | i_imm;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// beq and bne are handled differently
		// beq
		case BEQ_OP:
			branch_comp_result = sim->reg[rd] == sim->reg[rs];
			if (branch_comp_result)
				// -1 because pc has already been incremented
				sim->pc += i_imm - 1;
			// successful prediction
			if (branch_comp_result == branch_prediction(sim->p_method, sim->BPT))
			{
				sim->cycle_cnt += DEF_CYCLE_CNT;
				++sim->successful_prediction_cnt;
			}
			// unsuccessful prediction
			else
				sim->cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			++sim->total_prediction_cnt;
			sim->no_prediction_cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			// BPT update
			update_BPT(sim->p_method, sim->BPT, branch_comp_result);
			return;
		// bne (same as beq)
		case BNE_OP:
			branch_comp_result = sim->reg[rd] != sim->reg[rs];
			if (branch_comp_result)
				sim->pc += i_imm - 1;
			if (branch_comp_result == branch_prediction(sim->p_method, sim->BPT))
			{
				sim->cycle_cnt += DEF_CYCLE_CNT;
				++sim->successful_prediction_cnt;
			}
			else
				sim->cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			++sim->total_prediction_cnt;
			sim->no_prediction_cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			update_BPT(sim->p_method, sim->BPT, branch_comp_result);
			return;
		// bgt
		case BGT_OP:
			branch_comp_result = sim->reg[rd] > sim->reg[rs];
			if (branch_comp_result)
				// -1 because pc has already been incremented
				sim->pc += i_imm - 1;
			// successful prediction
			if (branch_comp_result == branch_prediction(sim->p_method, sim->BPT))
			{
				sim->cycle_cnt += DEF_CYCLE_CNT;
				++sim->successful_prediction_cnt;
			}
			// unsuccessful prediction
			else
				sim->cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			++sim->total_prediction_cnt;
			sim->no_prediction_cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			// BPT update
			update_BPT(sim->p_method, sim->BPT, branch_comp_result);
			return;
		// blt
		case BLT_OP:
			branch_comp_result = sim->reg[rd] < sim->reg[rs];
			if (branch_comp_result)
				// -1 because pc has already been incremented
				sim->pc += i_imm - 1;
			// successful prediction
			if (branch_comp_result == branch_prediction(sim->p_method, sim->BPT))
			{
				sim->cycle_cnt += DEF_CYCLE_CNT;
				++sim->successful_prediction_cnt;
			}
			// unsuccessful prediction
			else
				sim->cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			++sim->total_prediction_cnt;
			sim->no_prediction_cycle_cnt += UNSUCCESSFUL_PREDICTION_CYCLE_CNT;
			// BPT update
			update_BPT(sim->p_method, sim->BPT, branch_comp_result);
			return;
		// j-type
		// jmp
		case JMP_OP:
			sim->pc += j_imm - 1;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
		// jal
		case JAL_OP:
			sim->reg[7] = sim->pc;
			sim->pc += j_imm - 1;
			sim->cycle_cnt += DEF_CYCLE_CNT;
			sim->no_prediction_cycle_cnt += DEF_CYCLE_CNT;
			return;
	}
}

const int16_t branch_prediction(const enum prediction_method meth, int16_t* BPT)
{
	switch(meth)
	{
		// static true/taken
		case ST:
			return 1;
		// static false/not taken
		case SN:
			return 0;
		// dynamic, one-bit, two-state
		case D1:
			return BPT[0];
		// dynamic, two-bit, four-state
		case D2:
			return BPT[0];
		// dynamic, two-bit, three-state
		case IQ:
			return BPT[0];
		default:
			puts("invalid prediction method! (BP)");
			exit(EXIT_FAILURE);
	}
}

void update_BPT(const enum prediction_method meth, int16_t* BPT, const int16_t result)
{
	switch(meth)
	{
		// static true/taken
		case ST:
			// no change
			return;
		// static false/not taken
		case SN:
			// no change
			return;
		// dynamic, one-bit, two-state 
		case D1:
			BPT[0] = result;
			return;
		// dynamic, two-bit, four-state
		case D2:
			if(BPT[0] == result)
				BPT[1] = 1;
			else
			{
				if(BPT[1] == 0)
					BPT[0] = !BPT[0];
				else
					BPT[1] = 0;
			}
			return;
		// dynamic, two-bit, three-state
		case IQ:
			if(BPT[0] != result && BPT[1] == 0)
				BPT[0] = !BPT[0];
			else if(BPT[0] == 1 && (result != BPT[1]))
				BPT[1] = !BPT[1];
			return;
		default:
			puts("invalid prediction method! (UBPT)");
			exit(EXIT_FAILURE);
	}
}

void run(tb_sim* sim, const int16_t timeout, const enum prediction_method meth, const char* asm_file, const char* data_file, const char* report_file)
{
	// simulation starts
	clock_t start_time = clock(), end_time;
	refresh_sim(sim, meth);
	parse_asm_file(sim->memory, asm_file);
	parse_data_file(sim->memory, data_file);
	while(1)
	{
		// bug detection, yay :D
		if (sim->cycle_cnt > timeout)
		{
			puts("program timed out!");
			exit(EXIT_FAILURE);
		}
		int16_t instr = fetch(sim);
		// end of program
		if (instr == 0x0000)
			break;
		execute(sim, instr);
		++sim->instruction_cnt;
	}
	// simulation ends
	end_time = clock();
	sim->runtime = ((double)(end_time - start_time) * 1000) / CLOCKS_PER_SEC;
	report(sim, report_file);
}

void report(const tb_sim* sim, const char* report_file)
{
	FILE* file = fopen(report_file, "w");
	if (file == NULL)
	{
		puts("failed to open report file");
		exit(EXIT_FAILURE);
	}
	fputs("----------<tiny basu simulator report file>----------\n", file);
	fprintf(file, "simulation runtime: %.1fms\n", sim->runtime);
	fprintf(file, "number of instructions: %d\n", sim->instruction_cnt);
	fprintf(file, "number of simulation cycles: %d\n", sim->cycle_cnt);
	fprintf(file, "instructions per cycle: %.2f\n", (float)sim->instruction_cnt / (float)sim->cycle_cnt);
	fprintf(file, "number of stalls: %d\n", sim->stall_cnt);
	fprintf(file, "prediction accuracy: %%%.2f\n", (float)sim->successful_prediction_cnt / (float)sim->total_prediction_cnt * 100);
	fprintf(file, "speedup: %.1f\n", (float)(sim->no_prediction_cycle_cnt) / (float)(sim->cycle_cnt));
	fprintf(file, "\nprogram counter value: %d\n", sim->pc - 1);
	fputs("registers value:\n", file);
	for (int16_t i = 0; i < REG_CNT; ++i)
		fprintf(file, "reg[%d]:0x%X\n", i, sim->reg[i]);
	fputs("\nmemory content value:\n", file);
	for (int16_t i = 256; i < MEMORY_SIZE && sim->memory[i] != 0; ++i)
		fprintf(file, "memory[%d]:0x%X\n", i - 256, sim->memory[i]);
	fputs("others are 0x0", file);
	fclose(file);
}
