#include "tiny_basu_sim.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		puts("usage: [output_file] [total_cycles] [prediction_method] [inst_file] [data_file] [report_file]");
		return 0;
	}
	tb_sim sim;
	enum prediction_method meth;
	if (strcmp(argv[2], "ST") == 0)
		meth = ST;
	else if (strcmp(argv[2], "SN") == 0)
		meth = SN;
	else if (strcmp(argv[2], "D1") == 0)
		meth = D1;
	else if (strcmp(argv[2], "D2") == 0)
		meth = D2;
	else
		meth = IQ;
	run(&sim, atoi(argv[1]), meth, argv[3], argv[4], argv[5]);
}
