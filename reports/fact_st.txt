----------<tiny basu simulator report file>----------
simulation runtime: 0.2ms
number of instructions: 23
number of simulation cycles: 27
instructions per cycle: 0.85
number of stalls: 0
prediction accuracy: %83.33
speedup: 1.4

program counter value: 8
registers value:
reg[0]:0x0
reg[1]:0x1
reg[2]:0x13B0
reg[3]:0x0
reg[4]:0x0
reg[5]:0x0
reg[6]:0x0
reg[7]:0x0

memory content value:
memory[0]:0x13B0
others are 0x0