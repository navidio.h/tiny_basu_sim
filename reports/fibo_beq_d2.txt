----------<tiny basu simulator report file>----------
simulation runtime: 0.2ms
number of instructions: 116
number of simulation cycles: 120
instructions per cycle: 0.97
number of stalls: 0
prediction accuracy: %94.74
speedup: 1.3

program counter value: 13
registers value:
reg[0]:0x0
reg[1]:0xA18
reg[2]:0x1055
reg[3]:0x1A6D
reg[4]:0x14
reg[5]:0x0
reg[6]:0x0
reg[7]:0x0

memory content value:
memory[0]:0x1A6D
others are 0x0